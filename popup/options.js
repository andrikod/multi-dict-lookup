const lookUpDicts = {
    'pons.com': new PonsDict(),
    'dict.cc': new DictCCDict(),
    'deepl.com': new DeepDlDict(),
    'linguee.com': new LingueeDict(),
    'wordreference.com': new WordReferenceDict(),
    'langenscheidt.com': new LangenscheidtDict()
};

browser.storage.local.get(["dict", "combination", "url"]).then(configuration => {
    if (isEmpty(configuration)) {
        configuration = {
            dict: 'pons.com',
            combination: 'deen',
            url: lookUpDicts['pons.com'].getURL()
        };
        browser.storage.local.set(configuration);
    }
    // console.log('init::', configuration);
    return configuration;
}).then(configuration => {
    initialize(configuration);
});


function initialize(configuration) {
    for (let dict of Object.keys(lookUpDicts)) {
        let option = document.createElement("div");

        let optionInput = document.createElement("input");
        optionInput.setAttribute("id", dict);
        optionInput.setAttribute("type", "radio");
        optionInput.setAttribute("name", "dict-options");
        optionInput.setAttribute("value", dict);
        if (configuration.dict == dict) {
            optionInput.checked = true;
            createCombinations(dict, lookUpDicts[dict], configuration);
        }
        optionInput.addEventListener("change", function () {
            browser.storage.local.get(["dict", "combination"]).then(config => createCombinations(dict, lookUpDicts[dict], config));
        });

        let optionLabel = document.createElement("label");
        optionLabel.setAttribute("for", dict);
        optionLabel.innerText = dict;

        option.appendChild(optionInput);
        option.appendChild(optionLabel);

        document.getElementById("dict-options").appendChild(option);
    }
}


function createCombinations(dict, dictObj, configuration) {    
    let oldOption = document.getElementById("combinations");

    let newOption = document.createElement("div");
    newOption.setAttribute("id", "combinations");

    for (let i = 0; i < dictObj.combinations.length; i++) {
        let combination = dictObj.combinations[i];
        let option = document.createElement("div");

        let optionInput = document.createElement("input");
        optionInput.setAttribute("id", combination);
        optionInput.setAttribute("type", "radio");
        optionInput.setAttribute("name", "combinations");
        optionInput.setAttribute("value", combination);
        if (configuration.dict == dict && configuration.combination == combination) {
            optionInput.checked = true;            
        }

        optionInput.addEventListener("change", function () {
            browser.storage.local.get(["dict", "combination"]).then(config => {
                document.getElementById(config.dict).checked = false;
            }).then(() => {
                let config = {
                    dict: dict,                
                    combination: combination
                };
                document.getElementById(dict).checked = true;
                browser.storage.local.set(config);
            });
        });

        let optionLabel = document.createElement("label");
        optionLabel.setAttribute("for", combination);        
        optionLabel.innerText = dictObj.getLabelForCombination(combination);

        option.appendChild(optionInput);
        option.appendChild(optionLabel);

        newOption.appendChild(option);
    }

    if (oldOption) {
        document.getElementById("combi-options").replaceChild(newOption, oldOption);
    } else {
        document.getElementById("combi-options").appendChild(newOption);
    }

}

function isEmpty(obj) {
    for (var key in obj) {
        if (obj.hasOwnProperty(key))
            return false;
    }
    return true;
}