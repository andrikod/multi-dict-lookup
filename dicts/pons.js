class PonsDict {    
    constructor() {
        this.translationTerm = {
            'en': 'translate',
            'es': 'traducción',
            'de': 'übersetzung',
            'fr': 'traduction',
            'it': 'traduzione',
            'tr': 'çeviri',
            'zh': '翻译',
            'pl': 'tłumaczenie',
            'pt': 'tradução',
            'ru': 'перевод'
        };
    
        this.combinations = [
            'deen',
            'arde',
            'bgde',
            'dezh',
            'defr',
            'deel',
            'deit',
            'denl',
            'dept',
            'depl',
            'desl',
            'dees',
            'detr',
            'deru',
            'dela',
            'aren',
            'bgen',
            'enzh',
            'enfr',
            'enit',
            'enpl',
            'enpt',
            'enru',
            'ensl',
            'enes',
            'eszh',
            'esfr',
            'espl',
            'espt',
            'essl',
            'frzh',
            'frsl',
            'frpl',
            'itpl',
            'itsl',
            'plru'
        ];


        this.ponsLanguage = browser.i18n.getUILanguage().toLowerCase().substring(0, 2) || 'en';
        this.ponsTranslationTerm = this.translationTerm[this.ponsLanguage];        
    }
    
    getURL(term, combination) {
        return `https://${this.ponsLanguage}.pons.com/${this.ponsTranslationTerm}?q=${term}&l=${combination}`
    }

    getLabelForCombination(combination) {
        let langLeft = combination.substring(0, 2);
        let langRight = combination.substring(2, 4);
        return browser.i18n.getMessage(langLeft) + " - " + browser.i18n.getMessage(langRight);
    }

}