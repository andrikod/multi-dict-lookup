const lookUpDicts = {
  'pons.com': new PonsDict(),
  'dict.cc': new DictCCDict(),
  'deepl.com': new DeepDlDict(),
  'linguee.com': new LingueeDict(),
  'wordreference.com': new WordReferenceDict(),
  'langenscheidt.com': new LangenscheidtDict()
};

const searchMenuId = "pons-selection";


browser.menus.create({
  id: searchMenuId,
  title: 'Translate!',  //FIXME: i8
  contexts: ["selection"],
  "icons": {
    "16": "./icons/translate16.png",
    "32": "./icons/translate32.png"
  }
}, onCreated);


function onCreated() {
  if (browser.runtime.lastError) {
    console.log(`Error: ${browser.runtime.lastError}`);
  } else {
    console.log("Multi Dictionary Lookup context menu icon added.");
  }
  
  // clean up tab ID
  browser.storage.local.remove('translationTabId');
}

function onTranslate(configuration) {
  if (!term) {
    return
  }

  const urlString = lookUpDicts[configuration.dict].getURL(term, configuration.combination);
  console.log(urlString);

  browser.storage.local.get("translationTabId").then(tab => {        
    if (tab.translationTabId) {      
      browser.tabs.query({'currentWindow': true})
      .then(tabs => {        
        if (tabs.find((t) => t.id ===  tab.translationTabId)) {
          browser.tabs.update(tab.translationTabId, { active: true, url: urlString });
        } else {
          createNewTab(urlString);
        }
      });
    } else {
      createNewTab(urlString);
    }
  });
}

function createNewTab(urlString) {
  console.log('created tab');
  browser.tabs.create({
    active: true,
    url: urlString
  }).then(tab => {
    browser.storage.local.set({ 'translationTabId': tab.id });
  });
}

function onError(error) {
  console.log(`Error: ${error}`);
}

browser.menus.onClicked.addListener(function (info, tab) {
  if (info.menuItemId == searchMenuId) {
    term = info.selectionText.trim();
    browser.storage.local.get(["dict", "combination"]).then(configuration => onTranslate(configuration), onError);
  }
});


browser.commands.onCommand.addListener(command => {
  chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
    chrome.tabs.executeScript({ code: "window.getSelection().toString();" }, function (selection) {
      term = selection[0] || "";
      browser.storage.local.get(["dict", "combination"]).then(configuration => onTranslate(configuration), onError);
    });
  });

})
