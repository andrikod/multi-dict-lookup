<h1>Multi Dictionary LookUp Addon</h1>

<p>
Translate selected text at various online dictionaries.
Supported online dictionaries:
 - pons.com
 - linguee.com
 - dict.cc
 - deepl.com
 - wordreference.com
</p>


<h3>How to use</h3>
<p>
Select dictionary and language combination by clicking at the menubar icon.
Mark text and
 - right click and select <code>Tranlate!</code> on the menu
 - or press <code>Ctrl+Alt+T</code>.
</p>


<h4>How to manually install</h4>
<p>

- Download latest version from <code>web-ext-artifacts/</code> folder.

- Set <code>xpinstall.signatures.required=true</code> at Firefox <code>about:config</code> to be able to install unsigned addons.

- At Addons menu select Install From File and select the previously downloaded .zip file.
</p>



<div>
<p> Icons from: https://github.com/Remix-Design/remixicon
</p>
</div>