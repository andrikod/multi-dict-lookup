class WordReferenceDict {

    constructor() {
        this.combinations = [
            'ende',
            'deen',
            'engr',
            'gren',
            'enes',
            'esen',
            'esfr',
            'fres',
            'espt',
            'ptes',
            'esit',
            'ites',
            'esde',
            'dees',            
            'enfr',
            'fren',            
            'enit',
            'iten',                       
            'ennl',
            'nlen',
            'ensv',
            'sven',
            'enru',
            'ruen',
            'enpt',
            'pten',            
            'enpl',
            'plen',
            'enro',
            'roen',
            'encz',
            'czen',
            'entr',
            'tren',
            'enzh',
            'zhen',
            'enja',
            'jaen',
            'enar',
            'aren',
            'enko',
            'koen',            
        ];
    }

    getURL(term, combination) {
        let langLeft = combination.substring(0, 2);
        let langRight = combination.substring(2, 4);
        return `https://www.wordreference.com/${langLeft}${langRight}/${term}`;
    }


    getLabelForCombination(combination) {
        let langLeft = combination.substring(0, 2);
        let langRight = combination.substring(2, 4);
        return browser.i18n.getMessage(langLeft) + " > " + browser.i18n.getMessage(langRight);
    }

}