class LangenscheidtDict {    
    constructor() {

        // TODO: too complicated for now
        // this.subdomain = [
        //     'en',
        //     'es',
        //     'de',
        //     'fr',
        //     'it',
        //     'tr', 
        //     'pl',
        // ];

        this.langs = {
            'en': 'english',
            'de': 'german',
            'el': 'greek',
            'it': 'italian',
            'fr': 'french',
            'es': 'spanish',
            'pt': 'portoguese',
            'pl': 'polish',
            'nl': 'dutch',
            'tr': 'turkish',
        };
    
        this.combinations = [
            'deen',
            'ende',
            'deel',
            'elde',
            'deit',
            'itde',
            'defr',
            'frde',
            'dees',
            'esde',
            'dept',
            'ptde',
            'depl',
            'plde',
            'denl',
            'nlde',
            'detr',
            'trde',
        ];
        
    }
    
    getURL(term, combination) {        
        let langLeft = this.langs[combination.substring(0, 2)];
        let langRight = this.langs[combination.substring(2, 4)];
        return `https://en.langenscheidt.com/${langLeft}-${langRight}/search?term=${term}`
    }

    getLabelForCombination(combination) {
        let langLeft = combination.substring(0, 2);
        let langRight = combination.substring(2, 4);
        return browser.i18n.getMessage(langLeft) + " > " + browser.i18n.getMessage(langRight);
    }

}