class DictCCDict {

    constructor() {
        this.combinations = [
            'csde',
            'deen',
            'debg',
            'debs',
            'decs',
            'deda',
            'deel',
            'deeo',
            'dees',
            'defi',
            'defr',
            'dehr',
            'dehu',
            'deis',
            'deit',
            'dela',
            'denl',
            'deno',
            'depl',
            'dept',
            'dero',
            'deru',
            'desk',
            'desq',
            'desr',
            'desv',
            'detr',
            'enbg',
            'enbs',
            'encs',
            'enda',
            'enel',
            'eneo',
            'enes',
            'enfi',
            'enfr',
            'enhr',
            'enhu',
            'enis',
            'enit',
            'enla',
            'ennl',
            'enno',
            'enpl',
            'enpt',
            'enro',
            'enru',
            'ensk',
            'ensq',
            'ensr',
            'ensv',
            'entr'
        ];
    }

    getURL(term, combination) {
        return `https://${combination}.dict.cc/?s=${term}`
    }

    getLabelForCombination(combination) {
        let langLeft = combination.substring(0, 2);
        let langRight = combination.substring(2, 4);
        return browser.i18n.getMessage(langLeft) + " - " + browser.i18n.getMessage(langRight);
    }

}